//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./Ownerable.sol";
import "./Manager.sol";

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract Product is Ownerable, ReentrancyGuard {
    string public name;
    uint public quantity;
    uint public price;
    Manager private manager;

    struct Purchase {
        bool bought;
        uint blockNumber;
    }
    mapping(address => Purchase) public purchases;

    constructor(Manager _manager, address _owner, string memory _name, uint _quantity, uint _price) {
        manager = _manager;
        owner = _owner;
        name = _name;
        quantity = _quantity;
        price = _price;
    }

    receive() external payable {
        require(msg.value == price, "Price mismatch");
        require(purchases[msg.sender].bought == false, "Product already bought");
        require(quantity > 0, "Not in stock");

        payable(manager).transfer(address(this).balance);
        quantity--;
        purchases[msg.sender] = Purchase(true, block.number);
    }

    function refund() public nonReentrant {
        require(purchases[msg.sender].bought, "Product not bought");
        require(purchases[msg.sender].blockNumber + 100 >= block.number, "Out of block time");

        manager.refund(msg.sender, price);
        purchases[msg.sender].bought = false;
        quantity++;
    }

    function updateQuantity(uint _quantity) public onlyOwner {
        quantity = _quantity;
    }
}
