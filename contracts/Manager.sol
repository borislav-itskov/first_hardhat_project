//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./Ownerable.sol";
import "./Product.sol";

contract Manager is Ownerable {

    mapping(uint => Product) public products;
    mapping(address => bool) public productAddresses;
    mapping(string => bool) private productNames;
    uint public productIndex;

    function addProduct(string memory _name, uint _quantity, uint _price) public onlyOwner {
        require(productNames[_name] == false, "A product with this name already exists");

        Product product = new Product(
            this,
            address(msg.sender),
            _name,
            _quantity,
            _price
        );
        products[productIndex] = product;
        productAddresses[address(product)] = true;
        productNames[_name] = true;
        productIndex++;
    }

    function withdraw(uint _amount) public payable onlyOwner {
        require(address(this).balance >= _amount, "Insufficient funds");
        payable(msg.sender).transfer(_amount);
    }

    receive() external payable {
    }

    function getBalance() public onlyOwner view returns(uint) {
        return address(this).balance;
    }

    function refund(address _to, uint _price) public payable {
        require(productAddresses[msg.sender], "Unauthorized");
        require(address(this).balance >= _price, "Insufficient funds");
        payable(_to).transfer(_price);
    }

    function getProducts() public view returns(Product[] memory) {
        Product[] memory addedProducts = new Product[](productIndex);

        for (uint256 index = 0; index < productIndex; index++) {
            addedProducts[index] = products[index];
        }

        return addedProducts;
    }
}
