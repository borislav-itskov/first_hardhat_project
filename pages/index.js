import {useEffect, useState, setState, React} from 'react'
import getWeb3 from "./getWeb3";

import {managerAddress} from '../config'
import Manager from '../artifacts/contracts/Manager.sol/Manager.json';
import Product from '../artifacts/contracts/Product.sol/Product.json';

export default function Home() {
  const [managerContract, setManagerContract] = useState(null);
  const [accounts, setAccounts] = useState(null);
  const [products, setProducts] = useState([]);

  useEffect(async () => {
    const web3 = await getWeb3()

    let accounts = await web3.eth.getAccounts()
    setAccounts(accounts)

    let managerContract = new web3.eth.Contract(
      Manager.abi,
      managerAddress
    );
    setManagerContract(managerContract)

    let products = []
    let productAddresses = await managerContract.methods.getProducts().call({from: accounts[0]});
    console.log(productAddresses)

    productAddresses.forEach(async address => {
      let productContract = new web3.eth.Contract(
        Product.abi,
        address
      );

      products.push({
        name: await productContract.methods.name().call({from: accounts[0]}),
        quantity: await productContract.methods.quantity().call({from: accounts[0]}),
        price: await productContract.methods.price().call({from: accounts[0]})
      })
      setProducts(products);
    });
  });

  async function showProductIndex() {
    let productIndex = await managerContract.methods.productIndex().call({from: accounts[0]});
    console.log(productIndex)
  }

  function submitForm(e) {
    e.preventDefault()
    let form = document.getElementById('productForm')
    let elements = form.elements
    let name = elements['name'].value
    let quantity = elements['quantity'].value
    let price = elements['price'].value

    managerContract.methods.addProduct(name, quantity, price).send({from: accounts[0]});
  }

  const listProducts = products.map((product) =>
    <div key={product.name}>
      Name: {product.name} Quantity: {product.quantity} Price: {product.price}
    </div>
  );

  return (
    <div>
      <button onClick={showProductIndex}>productIndex</button>

      <form id='productForm'>
        <label for="name">Name</label>
        <input type="text" name="name" id="name" />
        <label for="quantity">Quantity</label>
        <input type="number" name="quantity" id="quantity" />
        <label for="price">Price in wei</label>
        <input type="number" name="price" id="price" />
        <input type="submit" onClick={submitForm} />
      </form>

      <h2>Displaying products</h2>
      {listProducts}
    </div>
  )
}
