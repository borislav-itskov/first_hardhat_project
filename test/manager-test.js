const { expect } = require("chai");
const { ethers, waffle } = require("hardhat");

describe("Manager", function () {
  it("Should add a product", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    const addProduct = await manager.addProduct("shirt", 5, 1000);

    // wait until the transaction is mined
    await addProduct.wait();

    let productAddress = await manager.products(0);
    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);

    expect(await product.name()).to.equal("shirt");
    expect(await product.quantity()).to.equal(5);
    expect(await product.price()).to.equal(1000);
  });
  it("Should NOT add products with none-owner accounts", async function () {
    const [owner, addr1] = await ethers.getSigners();
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    await expect(manager.connect(addr1).addProduct("jeans", 5, 1000))
      .to.be.revertedWith('Only owner allowed');
  });
  it("Should not add a product with the same name", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("shirt", 5, 1000);
    await addProduct.wait();

    // the second time, it should throw an error
    await expect(manager.addProduct("shirt", 5, 1000))
      .to.be.revertedWith('A product with this name already exists');
  });
  it("Should buy a product", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("shirt", 5, 1000);
    await addProduct.wait();

    // send a transaction from the owner
    let productAddress = await manager.products(0);
    const [owner] = await ethers.getSigners();

    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);
    await owner.sendTransaction({to: product.address, value: 1000});
    expect(await manager.getBalance()).to.equal(1000);
  });
  it("Should fail buying a product if the price does not match", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("shirt", 5, 1000);
    await addProduct.wait();

    // send a transaction from the owner
    let productAddress = await manager.products(0);
    const [owner] = await ethers.getSigners();

    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);

    await expect(owner.sendTransaction({to: product.address, value: 999}))
      .to.be.revertedWith('Price mismatch');
  });
  it("Should not be able to buy the same product twice", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("shirt", 5, 1000);
    await addProduct.wait();

    // send a transaction from the owner
    let productAddress = await manager.products(0);
    const [owner] = await ethers.getSigners();

    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);
    await owner.sendTransaction({to: product.address, value: 1000});

    // the second time, it should be reverted
    await expect(owner.sendTransaction({to: product.address, value: 1000}))
      .to.be.revertedWith('Product already bought');
  });
  it("Should refund a bought product", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("shirt", 5, 1000);
    await addProduct.wait();

    // send a transaction from the owner
    let productAddress = await manager.products(0);
    const [owner] = await ethers.getSigners();

    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);
    await owner.sendTransaction({to: product.address, value: 1000});
    expect(await manager.getBalance()).to.equal(1000);
    await product.refund();
    expect(await manager.getBalance()).to.equal(0);
  });
  it("Should not be able to buy the product if there is not enough quantity", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("shirt", 0, 1000);
    await addProduct.wait();

    // send a transaction from the owner
    let productAddress = await manager.products(0);
    const [owner] = await ethers.getSigners();

    await expect(owner.sendTransaction({to: productAddress, value: 1000}))
      .to.be.revertedWith('Not in stock');
  });
  it("Should be able to update the quantity", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("laptop", 20, 1000);
    await addProduct.wait();

    // send a transaction from the owner
    let productAddress = await manager.products(0);
    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);
    let updateQuantity = await product.updateQuantity(1);
    await updateQuantity.wait();
    expect(await product.quantity()).to.equal(1);
  });
  it("Should not refund a product from Manager.sol", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    // add a shirt product once
    const addProduct = await manager.addProduct("shirt", 5, 1000);
    await addProduct.wait();

    // send a transaction from the owner
    let productAddress = await manager.products(0);
    const [owner] = await ethers.getSigners();

    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);
    await owner.sendTransaction({to: product.address, value: 1000});
    expect(await manager.getBalance()).to.equal(1000);

    await expect(manager.refund(owner.address, 1000))
      .to.be.revertedWith('Unauthorized');
  });
});
