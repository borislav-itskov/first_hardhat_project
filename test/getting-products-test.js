const { expect } = require("chai");
const { ethers, waffle } = require("hardhat");

describe("Getting products", function () {
  it("Should get a product after adding it", async function () {
    const Manager = await ethers.getContractFactory("Manager");
    const manager = await Manager.deploy();
    await manager.deployed();

    const addProduct = await manager.addProduct("shirt", 5, 1000);

    // wait until the transaction is mined
    await addProduct.wait();

    let productAddress = await manager.products(0);
    const Product = await ethers.getContractFactory("Product");
    let product = await Product.attach(productAddress);

    expect(await product.name()).to.equal("shirt");
    expect(await product.quantity()).to.equal(5);
    expect(await product.price()).to.equal(1000);

    let allProducts = await manager.getProducts();
    console.log(allProducts);
  });
});
